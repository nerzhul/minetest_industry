#Drop items in-world on dig, does nothing in creative mode
item_drop.enable_item_drop (Enable item drops) bool true

#Use a key to pick up items
item_drop.enable_pickup_key (Use pickup key) bool true

#Collect items when the key is not pressed instead of when it is pressed
item_drop.pickup_keyinvert (Invert pickup key) bool true

#What keytype to use as pickup key
item_drop.pickup_keytype (Pickup keytype) enum Use Use,Sneak,LeftAndRight,RMB,SneakAndRMB

#The volume of the pickup sound
item_drop.pickup_sound_gain (Pickup sound gain) float 0.4

#Display a particle of the item picked up above the player
item_drop.pickup_particle (Pickup particle) bool true

#Player pickup radius, the maximum distance from which items can be collected
item_drop.pickup_radius (Pickup radius) float 0.75

#Time delay in seconds after autopicking an item if it's dropped by a player
item_drop.pickup_age (Pickup age) float 0.5

#Enable manual item pickups by mouse
item_drop.mouse_pickup (Mouse pickup) bool true
