
local function industry_main_step()
	local got_item
	local players = minetest.get_connected_players()
	for i = 1,#players do
		got_item = got_item or pickupfunc(players[i])
	end
	-- lower step if takeable item(s) were found
	local time
	if got_item then
		time = 0.02
	else
		time = 0.2
	end
	minetest.after(time, industry_main_step)
end
minetest.after(3.0, industry_main_step)