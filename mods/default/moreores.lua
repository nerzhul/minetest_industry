--[[
=====================================================================
** More Ores **
By Calinou, with the help of Nore.

Copyright © 2011-2020 Hugo Locurcio and contributors.
Licensed under the zlib license. See LICENSE.md for more information.
=====================================================================
--]]

moreores = {}

local S = minetest.get_translator("moreores")
moreores.S = S

local default_stone_sounds = default.node_sound_stone_defaults()
local default_metal_sounds = default.node_sound_metal_defaults()

-- Returns the crafting recipe table for a given material and item.
local function get_recipe(material, item)
	if item == "sword" then
		return {
			{material},
			{material},
			{"group:stick"},
		}
	end
	if item == "shovel" then
		return {
			{material},
			{"group:stick"},
			{"group:stick"},
		}
	end
	if item == "axe" then
		return {
			{material, material},
			{material, "group:stick"},
			{"", "group:stick"},
		}
	end
	if item == "pick" then
		return {
			{material, material, material},
			{"", "group:stick", ""},
			{"", "group:stick", ""},
		}
	end
	if item == "block" then
		return {
			{material, material, material},
			{material, material, material},
			{material, material, material},
		}
	end
	if item == "lockedchest" then
		return {
			{"group:wood", "group:wood", "group:wood"},
			{"group:wood", material, "group:wood"},
			{"group:wood", "group:wood", "group:wood"},
		}
	end
end

local function add_ore(modname, description, mineral_name, oredef)
	local img_base = modname .. "_" .. mineral_name
	local toolimg_base = modname .. "_tool_"..mineral_name
	local tool_base = modname .. ":"
	local tool_post = "_" .. mineral_name
	local item_base = tool_base .. mineral_name
	local ingot = item_base .. "_ingot"
	local lump_item = item_base .. "_lump"

	minetest.register_node(modname .. ":mineral_" .. mineral_name, {
		description = S("@1 Ore", S(description)),
		tiles = {"default_stone.png^" .. modname .. "_mineral_" .. mineral_name .. ".png"},
		groups = {cracky = 2},
		sounds = default_stone_sounds,
		drop = lump_item,
	})

	local block_item = item_base .. "_block"
	minetest.register_node(block_item, {
		description = S("@1 Block", S(description)),
		tiles = {img_base .. "_block.png"},
		groups = {snappy = 1, bendy = 2, cracky = 1, melty = 2, level = 2},
		sounds = default_metal_sounds,
	})
	minetest.register_alias(mineral_name.."_block", block_item)
	minetest.register_craft( {
		output = block_item,
		recipe = get_recipe(ingot, "block")
	})
	minetest.register_craft( {
		output = ingot .. " 9",
		recipe = {
			{block_item},
		}
	})

	minetest.register_craftitem(lump_item, {
		description = S("@1 Lump", S(description)),
		inventory_image = img_base .. "_lump.png",
	})
	minetest.register_alias(mineral_name .. "_lump", lump_item)
	minetest.register_craft({
		type = "cooking",
		output = ingot,
		recipe = lump_item,
	})

	minetest.register_craft( {
		output = "default:chest_locked",
		recipe = {
			{ingot},
			{"default:chest"},
		}
	})
	minetest.register_craft( {
		output = "default:chest_locked",
		recipe = get_recipe(ingot, "lockedchest")
	})

	oredef.oredef.ore_type = "scatter"
	oredef.oredef.ore = modname .. ":mineral_" .. mineral_name
	oredef.oredef.wherein = "default:stone"

	minetest.register_ore(oredef.oredef)

	for tool_name, tooldef in pairs(oredef.tools) do
		local tdef = {
			description = "",
			inventory_image = toolimg_base .. tool_name .. ".png",
			tool_capabilities = {
				max_drop_level = 3,
				groupcaps = tooldef,
			},
			sound = {breaks = "default_tool_breaks"},
		}

		if tool_name == "sword" then
			tdef.tool_capabilities.full_punch_interval = oredef.full_punch_interval
			tdef.tool_capabilities.damage_groups = oredef.damage_groups
			tdef.description = S("@1 Sword", S(description))
		end

		if tool_name == "pick" then
			tdef.tool_capabilities.full_punch_interval = oredef.full_punch_interval
			tdef.tool_capabilities.damage_groups = oredef.damage_groups
			tdef.description = S("@1 Pickaxe", S(description))
		end

		if tool_name == "axe" then
			tdef.tool_capabilities.full_punch_interval = oredef.full_punch_interval
			tdef.tool_capabilities.damage_groups = oredef.damage_groups
			tdef.description = S("@1 Axe", S(description))
		end

		if tool_name == "shovel" then
			tdef.full_punch_interval = oredef.full_punch_interval
			tdef.tool_capabilities.damage_groups = oredef.damage_groups
			tdef.description = S("@1 Shovel", S(description))
			tdef.wield_image = toolimg_base .. tool_name .. ".png^[transformR90"
		end

		local fulltool_name = tool_base .. tool_name .. tool_post

		minetest.register_tool(fulltool_name, tdef)

		minetest.register_craft({
			output = fulltool_name,
			recipe = get_recipe(ingot, tool_name)
		})

		-- Toolranks support
		if minetest.get_modpath("toolranks") then
			minetest.override_item(fulltool_name, {
				original_description = tdef.description,
				description = toolranks.create_description(tdef.description, 0, 1),
				after_use = toolranks.new_afteruse})
		end

		minetest.register_alias(tool_name .. tool_post, fulltool_name)
	end
end

local oredefs = {
	silver = {
		description = "Silver",
		oredef = {
			clust_scarcity = 11 ^ 3,
			clust_num_ores = 4,
			clust_size = 11,
			y_min = -31000,
			y_max = 50,
		},
		tools = {
			pick = {
				cracky = {times = {[1] = 2.60, [2] = 1.00, [3] = 0.60}, uses = 100, maxlevel = 1},
			},
			shovel = {
				crumbly = {times = {[1] = 1.10, [2] = 0.40, [3] = 0.25}, uses = 100, maxlevel = 1},
			},
			axe = {
				choppy = {times = {[1] = 2.50, [2] = 0.80, [3] = 0.50}, uses = 100, maxlevel = 1},
				fleshy = {times = {[2] = 1.10, [3] = 0.60}, uses = 100, maxlevel = 1}
			},
			sword = {
				fleshy = {times = {[2] = 0.70, [3] = 0.30}, uses = 100, maxlevel = 1},
				snappy = {times = {[2] = 0.70, [3] = 0.30}, uses = 100, maxlevel = 1},
				choppy = {times = {[3] = 0.80}, uses = 100, maxlevel = 0},
			},
		},
		full_punch_interval = 1.0,
		damage_groups = {fleshy = 6},
	}
}

for orename, def in pairs(oredefs) do
	-- Register everything
	add_ore("default", def.description, orename, def)
end
